clc
% hold off
% plotContours(@func_1a);
% hold on
x0 = [5;0];
x1 = [-2;4];
x2 = [1;2];
current_x = x2;
fprintf('Taking the initial point to be:\n');
disp(current_x);
fprintf('Tolerance: '); disp(1e-6);
fprintf('Iterations: '); disp(20);
%[x,f] = newtonSys(@func_1a, @fdJacobian, current_x, 1e-14, 20);
[x,f] = newtonSysLine(@func_1a, @fdJacobian, current_x, 1e-6, 20);
