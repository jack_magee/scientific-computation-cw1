Taking the initial point to be:
     5
     0

Tolerance:    1.0000e-14

Iterations:     20

 x      |f(x)|
 0    10.292360
 1     2.546259
 2     0.049945
 3     0.000023
 4     0.000000
 5     0.000000
 SUCCESS: Converged