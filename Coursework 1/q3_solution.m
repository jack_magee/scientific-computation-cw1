Taking the initial point to be:
     5
     4
     3
     2
     1

Tolerance:    1.0000e-12

Iterations:     20

 x      |f(x)|
 0     1.844449
 1     0.228048
 2     0.001538
 3     0.000000
 4     0.000000
 SUCCESS: Converged
x

x =

    5.4844
    5.0476
    4.6732
    4.3490
    4.0656