function [y] = func_1a(x)
    y = zeros(2, size(x,2));
    y(1,:) = x(1,:).^2 + x(2,:).^2 + exp(-x(1,:)) - 30;
    y(2,:) = -x(2,:).^2 + x(1,:).*x(2,:) + exp(x(2,:)) - 10;
end