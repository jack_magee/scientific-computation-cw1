clc
x0 = [50;50];
x1 = [100;100];
x2 = [300;300];
current_x = x2;
fprintf('Taking the initial point to be:\n');
disp(current_x);
fprintf('Tolerance: '); disp(1e-6);
fprintf('Iterations: '); disp(20);
[x,f] = newtonSys(@func_2a, @fdJacobian, current_x, 1e-6, 20);
%For part c, using my correct_loc, can see correct root is when starting at
%(300,300).

