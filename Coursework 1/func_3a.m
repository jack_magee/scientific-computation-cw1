function [F] = func_3a(U)
%Where U_i is a_i
    a_0 = 6;
    k = 0.6;
    V = 1;
    G = 35;
    beta = k*V/G;
    F = zeros(5, size(U,2));
    F(1,:) = beta*U(1,:).^2 + U(1,:) - a_0;
    F(2,:) = beta*U(2,:).^2 + U(2,:) - U(1,:);
    F(3,:) = beta*U(3,:).^2 + U(3,:) - U(2,:);
    F(4,:) = beta*U(4,:).^2 + U(4,:) - U(3,:);
    F(5,:) = beta*U(5,:).^2 + U(5,:) - U(4,:);
end