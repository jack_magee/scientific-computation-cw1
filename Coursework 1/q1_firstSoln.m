Taking the initial point to be:
    -2
     4

Tolerance:    1.0000e-14

Iterations:     20

 x      |f(x)|
 0    20.762967
 1     4.473534
 2     0.416998
 3     0.005060
 4     0.000001
 5     0.000000
 6     0.000000
 SUCCESS: Converged