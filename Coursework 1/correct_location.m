function [dist] = correct_location(soln)
    dist = zeros(3,1);
    dist(1) = sqrt((200-soln(1))^2 + (150-soln(2))^2);
    dist(2) = sqrt((-soln(1))^2 + (150-soln(2))^2);
    dist(3) = sqrt((-soln(1))^2 + (-150-soln(2))^2);
end