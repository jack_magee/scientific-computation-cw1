function out = q3_jacobian(a)
    k = 0.6;
    V = 1;
    G = 35;
    beta = k*V/G;
out = [2*beta*a(1) + 1, 0, 0, 0, 0;
    -1, 2*beta*a(2) + 1, 0, 0, 0;
    0, -1, 2*beta*a(3) + 1, 0, 0;
    0, 0, -1, 2*beta*a(4) + 1, 0;
    0, 0, 0, -1, 2*beta*a(5) + 1];
end