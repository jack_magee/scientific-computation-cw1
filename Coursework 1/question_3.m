clc
x0 = [5;4;3;2;1];
current_x = x0;
fprintf('Taking the initial point to be:\n');
disp(current_x);
fprintf('Tolerance: '); disp(1e-12);
fprintf('Iterations: '); disp(20);
[x,f] = newtonSys_q3(@func_3a, @q3_jacobian, current_x, 1e-12, 20);
%For the initial condition, we need to have x_0>x_1>x_2...
%a Suitable start point would be [5;4;3;2;1].
